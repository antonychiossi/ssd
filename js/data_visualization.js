
function drawGAPGraph(obj_json, response, network_id) {
    if(settings.vis_sol == false){
        console.log("graph Disabled");
        return
    }else{
        console.log("graph Enabled");
    }
    console.log(response);

    var obj_sol = response.result;
    var exec_time = response.exec_time;

    var json = obj_json;
    var sol = obj_sol.sol;
    var n_stores = Math.max.apply(Math, sol) + 1;
    var n_costumers = sol.length;

    var capacities = Array.apply(null, new Array(n_stores)).map(Number.prototype.valueOf,0);
    for(var k=0; k < sol.length; k++){
        var mag = sol[k];
        capacities[mag] += json.requests[mag][k];
    }

    var nodes = [];
    var edges = [];
    for(var s=0; s < n_stores; s++){
        var store = {
            id: s,
            label: "warehouse_" + s + " [" + capacities[s] + "/" + json.capacities[s] + "]",
            group: 'warehouses'
        };
        nodes.push(store);
    }
    for(var c=0; c < n_costumers; c++){
        var customer = {
            id: 100 + c,
            label: "customer_" + c,
            group: 'customers'
        };
        nodes.push(customer);

        var line = {
            from: sol[c],
            to: customer.id
        };
        edges.push(line);
    }




// create a network
//     console.log(network_id);
    var container = document.getElementById(network_id);
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 20,
            font: {
                size: 15,
                color: '#ffffff'
            },
            borderWidth: 2
        },
        edges: {
            width: 2,
            color: '#a3a3a3',
            dashes: true
        },
        groups: {
            diamonds: {
                color: {background:'red',border:'white'},
                shape: 'diamond'
            },
            dotsWithLabel: {
                label: "I'm a dot!",
                shape: 'dot',
                color: 'red'
            },
            mints: {color:'rgb(0,255,140)'},
            warehouses:{
                shape: 'icon',
                icon: {
                    face: 'FontAwesome',
                    code: '\uf0d1',
                    size: 50,
                    color: 'orange'
                },
                font: {
                    size: 20,
                    color: '#fff',
                    background: '#000'
                }
            },
            customers: {
                shape: 'icon',
                icon: {
                    face: 'FontAwesome',
                    code: '\uf0c0',
                    size: 50,
                    color: '#3b5998'
                }
            },
            source: {
                color:{border:'white'}
            }
        }
    };
    var network = new vis.Network(container, data, options);

    var html = '<div class="network-left-text">' +
        '<span id="cost_const_heur">Cost: ' + obj_sol.cost + '</span><br>' +
        '<span id="cost_const_heur">Time: ' + exec_time + ' ms</span>' +
        '</div>' +
        '<div class="network-right-magnify">' +
        '<i id="magnify_'+network_id+'" class="fa fa-search-plus" aria-hidden="true"></i>' +
        '</div>';
    $(container).append(html);
    //$("#cost_"+network_id).html("Cost: " + obj_sol.cost);
}




/*
*  --------- Manager ----------
*/

var graph_container = $('.tab-content');
var table_container = $('#table_container').html();

var ids = ["#const_heur", "#local-1-0", "#local-1-1", "#sa-1-0", "#sa-1-1", "#tabu-1-0", "#ils-1-0", "#ils-1-1"];
$.each(ids, function(index, value) {
    magnify(value);
    close(value);
});

function magnify(network_id) {

    var concat_net_id = network_id.replace("#", "");
    var net = $(network_id);
    //console.log(net);
    var cost = $("#cost_" + concat_net_id);
    graph_container.on('click', "#magnify_" + concat_net_id, function() {
        // console.log("#magnify_" + concat_net_id);
        net.css("height", "auto");
        net.addClass("full-page");
        cost.removeClass(concat_net_id + '-up-left').addClass('up-left-close');
        $(this).attr("id", "close_" + concat_net_id);
        $(this).removeClass(concat_net_id + '-up-right').addClass('up-right-close');
        $(this).removeClass('right-magnify');
        cost.removeClass('left-cost');
        $(this).removeClass('fa-search-plus').addClass('fa-times');
    });
}

function close(network_id) {
    var concat_net_id = network_id.replace("#", "");
    var net = $(network_id);
    var cost = $("#cost_" + concat_net_id);
    graph_container.on('click', "#close_" + concat_net_id, function() {
        // console.log("#close_" + concat_net_id);
        net.removeClass('full-page');
        net.css("height", "100%");
        cost.removeClass('up-left-close').addClass(concat_net_id + '-up-left');
        $(this).attr("id", "magnify_" + concat_net_id);
        $(this).removeClass('up-right-close').addClass(concat_net_id + '-up-right');
        $(this).addClass('right-magnify');
        cost.addClass('left-cost');
        $(this).removeClass('fa-times').addClass('fa-search-plus');
    });
}

