/**
 * Created by heis on 11/28/16.
 */
function getFilename(url){
    url = url.split('/').pop().replace(/\#(.*?)$/, '').replace(/\?(.*?)$/, '');
    return url
}

$( document ).ready(function() {
    var li = '<li><a href="#" onclick="readJson(\'#url\', false)">#name</a></li>';
    var base_url = "http://astarte.csr.unibo.it/gapdata/";

    $.get("http://astarte.csr.unibo.it/gapdata/gapinstances.html", function(data){
        var html = $(data);
        var matches = $(html).find('[href]');
        var hrefs = [];
        $.each(matches, function(i, el){
            var h = $(el).attr('href');
            if( h.indexOf(".json") > 0){
                var url = base_url + $(el).attr('href');
                var fname = getFilename(url);

                var lifile = li.replace("#name", fname).replace("#url", url).replace("\\","/");
                $('#url-selector').append(lifile);
            }
        });
    });
});

var settings = {
    local_10: false,
    local_11: false,
    sa_10: false,
    sa_11: false,
    tabu_10: false,
    ils_10: false,
    ils_11: false,
    vns_11: false,
    grasp_11: false,
    vis_sol: false
};

var workers = [];
var scheduler = [];
var n_cores = navigator.hardwareConcurrency;
var exec_params;

/**
 * Add a new worker to the scheduler
 * @param task = js file to be executed by worker
 * @param solution = a given solution
 * @param instance = json representing an instance of a problem
 */
function addWorker(task, solution, instance){

    var idx = workers.length;
    var path = "algorithms/";
    var copy_sol = jQuery.extend(true, {}, solution);
    var table_body = $('#table_body');

    console.log("[add] idx: " + idx + " task: "+ task.path);

    workers.push(
        new Worker(path + task.path)
    );

    workers[idx].postMessage({solution: copy_sol, instance: instance, opt: task.opt, exec_params: exec_params});
    console.log("[wlen]" + workers.length);

    workers[idx].onmessage = function(event) {
        // console.log("from: " + event.data.task + " res: " + event.data.result.toSource());

        workers[idx] = undefined;
        console.log(event.data);
        var cost = event.data.result.cost;
        var time = event.data.exec_time;

        switch (event.data.task){
            case "10opt":
                table_body.append("<tr><td>Local 1-0</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "local-1-0");
                break;
            case "11opt":
                table_body.append("<tr><td>Local 1-1</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "local-1-1");
                break;
            case "sa-10":
                table_body.append("<tr><td>Simulated annealing 1-0</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "sa-1-0");
                break;
            case "sa-11":
                table_body.append("<tr><td>Simulated annealing 1-1</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "sa-1-1");
                break;
            case "tabu":
                table_body.append("<tr><td>Tabu</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "tabu-1-0");
                break;
            case "ils-10":
                table_body.append("<tr><td>Iterated local search 1-0</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "ils-1-0");
                break;
            case "ils-11":
                table_body.append("<tr><td>Iterated local search 1-1</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "ils-1-1");
                break;
            case "vns-11":
                table_body.append("<tr><td>Variable neighborhood search</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "vns-1-1");
                break;
            case "grasp-11":
                table_body.append("<tr><td>Grasp</td><td>"+cost+"</td><td>"+time+"</td></tr>");
                drawGAPGraph(instance, event.data, "grasp-1-1");
                break;
        }

        if(event.data.result != undefined) {
            if (scheduler.length > 0) {
                var task = scheduler.pop();
                addWorker(task, copy_sol, instance);
            }
        }

        var all_task_completed = true;
        $.each(workers, function(index, value) {
            if(value != undefined){
                return all_task_completed = false;
            }
        });
        // console.log(workers);
        if(all_task_completed){
            console.log("------- all_task_completed -------");
            var table = $('#example');
            $('#please_wait').css("display", "none");
            table.DataTable();
            $('html, body').animate({
                scrollTop: table.offset().top
            }, 2000);
        }
    };
}

function startWorker(scheduler, solution, instance) {

    if(typeof(Worker) !== "undefined") {
        for(var c=0; c < n_cores && scheduler.length > 0 ; c++){
            if(workers.length < n_cores){
                var task = scheduler.pop();
                addWorker(task, jQuery.extend(true, {}, solution), instance);
            }
        }
    } else {
        document.getElementById("result").innerHTML = "Sorry! No Web Worker support.";
    }
}

function readJson(file, absolute_path) {

    workers = [];
    scheduler = [];
    var max_iters = $('#iterations').val();
    var max_time = $('#max_time').val();
    exec_params = {iters: max_iters, time: max_time};

    // display network
    if(settings.vis_sol == true){
        $("#const_heur").css("display", "block");
    }else{
        $("#const_heur").css("display", "none");
    }

    var url = file;
    if(!absolute_path){
        var input = $('#input').val(url);
    }

    $.getJSON( url, function( data ) {

        var data = {
            n_stores: data.numfacilities,
            n_customers: data.numcustomers,
            costs: data.cost,
            requests: data.req,
            capacities: data.cap
        };

        var loading = $("#loading").html();

        var sol = constrHeu(data);
        drawGAPGraph(data, {result: sol}, "const_heur");
        // scheduler.push({path: "solutionBuilders/constrHeu.js", opt: -1});

        if(settings.local_10){
            // $("#local-1-0").html(loading);
            scheduler.push({path: "locals/10opt.js", opt: undefined});
        }

        if(settings.local_11) {
            // $("#local-1-1").html(loading);
            scheduler.push({path: "locals/11opt.js", opt: undefined});
        }

        if(settings.sa_10) {
            // $("#sa-1-0").html(loading);
            scheduler.push({path: "metaheuristics/SA.js", opt: 10});
        }

        // if(settings.sa_11) {
        //     // $("#sa-1-1").html(loading);
        //     scheduler.push({path: "metaheuristics/SA.js", opt: 11});
        // }

        if(settings.tabu_10) {
            // $("#tabu-1-0").html(loading);
            scheduler.push({path: "metaheuristics/tabu.js", opt: undefined});
        }

        if(settings.ils_10) {
            // $("#ils-1-0").html(loading);
            scheduler.push({path: "metaheuristics/ILS.js", opt: 10});
        }

        if(settings.ils_11) {
            // $("#ils-1-1").html(loading);
            scheduler.push({path: "metaheuristics/ILS.js", opt: 11});
        }

        if(settings.vns_11) {
            // $("#ils-1-1").html(loading);
            scheduler.push({path: "metaheuristics/vns.js", opt: 11});
        }

        if(settings.grasp_11) {
            // $("#ils-1-1").html(loading);
            scheduler.push({path: "metaheuristics/grasp.js", opt: 11});
        }

        if(scheduler.length > 0){
            console.log("please wait");
            //please wait
            $('#please_wait').css("display", "block");

            //reset table
            var table = $('#table_container');
            table.html(table_container);
            table.css("display", "inline");
        }
        startWorker(scheduler, sol, data);
    });
    //console.log(settings);
}



/* --------- CHECKBOXES --------- */

$(function() {
    $( "input[type*='checkbox']" ).each(function () {
        $(this).change(function() {

            var id_split = $(this).attr("id").split("-");
            var tag = id_split[1];
            var opt = "-" + id_split[2] + "-" + id_split[3];
            var opt_setting = "_" + id_split[2]+id_split[3];

            if($(this).prop('checked')){
                if($(this).attr("id") == "vis-sol"){
                    settings["vis_sol"] = true;
                    $.each( settings, function( key, value ) {
                        var split = key.split("_");
                        var tag = split[0];
                        var opt = "-" + split[1].substring(1,0) + "-" + split[1].substring(1,2);
                        // console.log(tag, opt);
                        if(value == true){
                            console.log(tag, value);
                            $($("li[id=tab-" + tag + "]")[0]).css("display", "inline");
                            $("#div-" + tag + opt).css("display", "inline");
                        }
                    });
                }else{
                    settings[tag + opt_setting] = true;
                }
                if(settings.vis_sol == true) {
                    $($("li[id=tab-" + tag + "]")[0]).css("display", "inline");
                    $("#div-" + tag + opt).css("display", "inline");
                }
            /* when NOT checked */
            }else{
                if($(this).attr("id") == "vis-sol"){
                    settings["vis_sol"] = false;
                    hideAllTabs();
                }else{
                    settings[tag+opt_setting] = false;
                }
                var is_checked = false;
                $( "input[id^='check-" + tag +"']").each(function () {
                    if(this.checked == true){
                        return is_checked = true;
                    }
                });
                if(is_checked == false){
                    $($( "li[id=tab-"+tag+"]" )[0]).css("display", "none");
                }
                $( "#div-"+tag+opt).css("display", "none");
            }
            // console.log(tag, opt);
            // console.log(settings);
        });
    });
});

function hideAllTabs(tag, opt){
    $( "li[id^='tab-']").each(function () {
       $(this).css("display", "none");
    });
}
