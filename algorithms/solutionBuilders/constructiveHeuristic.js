
var CONSTANT_KEY = 0;
var CONSTANT_VALUE = 1;

function constrHeu(instance) {

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    var costs = instance.costs;
    var requests = instance.requests;
    var capacities = instance.capacities;

    var customers_indexes = new Array(n_customers);

    var i,j;

    for(j=0; j<n_customers; j++){
        customers_indexes[j] = j;
    }

    var solution = new Array(n_customers);

    var stores_requests_sum = [];
    for (i = 0; i < n_stores; i++) {
        stores_requests_sum[i] = 0;
    }

    var z=0;

    for (j = 0; j < n_customers; j++) {

        var current_cust_index = customers_indexes[j];

        var requests_customer = new Array(n_stores);
        for (i = 0; i < n_stores; i++) {
            requests_customer[i] = new Array(2);
            requests_customer[i][CONSTANT_KEY] = i;
            requests_customer[i][CONSTANT_VALUE] = requests[i][current_cust_index];
        }

        // ordino in modo ascendente
        requests_customer.sort(function (a, b) {
            return (a[CONSTANT_VALUE] ==  b[CONSTANT_VALUE]) ? 0 : (a[CONSTANT_VALUE] < b[CONSTANT_VALUE]) ? -1 : 1;
        });

        // avendo le richieste ordinate per richiesta minore le scorro assegnandole al primo mag. libero
        for (i = 0; i < n_stores; i++) {
            var realIndexStore = requests_customer[i][CONSTANT_KEY];
            if (stores_requests_sum[realIndexStore] + requests[realIndexStore][current_cust_index] <= capacities[realIndexStore]) {

                solution[current_cust_index] = realIndexStore;

                stores_requests_sum[realIndexStore] += requests[realIndexStore][current_cust_index];
                z += costs[realIndexStore][current_cust_index];
                break;
            }
        }

    }

    return {
        sol: solution,
        cost: z,
        stores_req_sum: stores_requests_sum
    };
}

