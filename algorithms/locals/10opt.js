
// self.addEventListener("message", function (params) {
//     if(params.solution != undefined && params.instance != undefined){
//         gap10opt(params.solution, params.instance);
//     }
// });


importScripts('https://cloud.github.com/downloads/kpozin/jquery-nodom/jquery.nodom.js');

this.onmessage = function(params) {
    if(params.data.solution != undefined && params.data.instance != undefined){
        var t0 = performance.now();
        var sol = gap10opt(params.data.solution, params.data.instance);
        var t1 = performance.now();
        postMessage({task: "10opt", result: sol, exec_time: (t1 - t0)});
    }
};

function gap10opt(solution, instance){

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    var costs = instance.costs;
    var req = instance.requests;
    var cap = instance.capacities;

    //console.log(n_customers);
    do{
        var improved = false;

        $.each(new Array(n_customers), function(index, value) {

                var cust_index = index;
                var current_store = solution.sol[cust_index];
                    //console.log("index= "+ index);

                $.each(new Array(n_stores), function(index, value) {
                    var store_index = index;
                    //console.log(value+ "===" + current_store);
                    if(value == current_store) {
                        return true;
                    }

                    var new_cost = solution.cost - costs[current_store][cust_index] + costs[store_index][cust_index];

                    if(solution.stores_req_sum[store_index]+req[store_index][cust_index] <= cap[store_index] && new_cost < solution.cost){

                        solution.sol[cust_index] = store_index;
                        improved = true;

                        solution.stores_req_sum[current_store] -= req[current_store][cust_index];
                        solution.cost = new_cost;
                        solution.stores_req_sum[store_index] += req[store_index][cust_index];
                        return false;
                    }
                });

                if(improved){
                    return false;
                }
        });

    } while(improved);

    //console.log(solution);
    return solution;
}