
if( 'function' === typeof importScripts) {
    importScripts('../locals/10opt.js');
    importScripts('https://cloud.github.com/downloads/kpozin/jquery-nodom/jquery.nodom.js');
    this.onmessage = function (params) {

        if (params.data.solution != undefined && params.data.instance != undefined) {
            var t0 = performance.now();
            // var sol = executeOpts(params.data.solution, params.data.instance);
            var sol = executeOpts(params.data.solution, params.data.instance);
            var t1 = performance.now();
            postMessage({task: "11opt", result: sol, exec_time: (t1 - t0)});
        }
    };
}


function executeOpts(solution, instance) {

    do {
        var current_cost = solution.cost;
        solution = gap10opt(solution, instance);
        solution = intersectMoves(solution, instance);

    } while (solution.cost < current_cost);

    return solution;

}

function intersectMoves(solution, instance) {

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    var costs = instance.costs;
    var requests = instance.requests;
    var capacities = instance.capacities;

    do {
        var improved = false;

        $.each(new Array(n_customers), function(index, value) {
            var couple_index1 = index;
            if(index >= n_customers - 1)
                return false;

            var current_store_c1 = solution.sol[couple_index1];

            $.each(new Array(n_customers), function(index, value) {
                var couple_index2 = index;
                if(index < couple_index1 + 1)
                    return true;

                if(index >= n_customers)
                    return false;

                var currentStoreK = solution.sol[couple_index2];

                // se sono nello stesso store: continue;
                if (current_store_c1 == currentStoreK) {
                    return true;
                }

                // provo riassegnamento

                var newCost =   solution.cost
                    - costs[current_store_c1][couple_index1] + costs[currentStoreK][couple_index1]
                    - costs[currentStoreK][couple_index2] + costs[current_store_c1][couple_index2];

                if (solution.stores_req_sum[current_store_c1] - requests[current_store_c1][couple_index1] + requests[current_store_c1][couple_index2] <= capacities[current_store_c1] &&
                    solution.stores_req_sum[currentStoreK] - requests[currentStoreK][couple_index2] + requests[currentStoreK][couple_index1] <= capacities[currentStoreK] &&
                    newCost < solution.cost) {

                    solution.stores_req_sum[current_store_c1] += -requests[current_store_c1][couple_index1] + requests[current_store_c1][couple_index2];
                    solution.stores_req_sum[currentStoreK] += -requests[currentStoreK][couple_index2] + requests[currentStoreK][couple_index1];

                    solution.sol[couple_index1] = currentStoreK;
                    solution.sol[couple_index2] = current_store_c1;

                    solution.cost = newCost;

                    improved = true;
                    return false;
                }

            });

            if (improved) {
                return false;
            }
        });

    } while (improved);


    return solution;




}