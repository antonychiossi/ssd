/**
 * Created by heis on 12/10/16.
 */
var CONSTANT_KEY = 0;
var CONSTANT_VALUE = 1;

var aaaa;
var etime;
var eiters;
var timer = {};

function isTimeOver() {
    timer.stop = performance.now();
    return timer.stop - timer.start < etime;
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

if( 'function' === typeof importScripts) {
    importScripts('../locals/10opt.js', '../locals/11opt.js');

    this.onmessage = function (params) {
        if (params.data.solution != undefined && params.data.instance != undefined) {

            etime = params.data.exec_params.time;
            eiters = params.data.exec_params.iters;
            timer.start = performance.now();

            var t0 = performance.now();
            var sol = gaspr(params.data.solution, params.data.instance);
            var t1 = performance.now();

            console.log(sol);
            postMessage({task: "grasp-11", result: sol, exec_time: (t1 - t0)});
        }
    };
}

function gaspr(sol, instance) {

    var k = 5;
    do{
        var sol_grasp = constrGrasp(instance, k);
        if(isFeasible(sol_grasp, instance) && sol_grasp.cost < sol.cost){
            sol = clone(sol_grasp);
        }
        var sol_10 = gap10opt(sol, instance);
        if(sol_10.cost < sol.cost){
            sol = clone(sol_10);
        }
        var sol_11 = intersectMoves(sol, instance);
        if(sol_11.cost < sol.cost){
            sol = clone(sol_11);
        }
    }while(eiters && isTimeOver());

    return sol;
}

function constrGrasp(instance, k) {

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    var costs = instance.costs;
    var requests = instance.requests;
    var capacities = instance.capacities;

    var customers_indexes = new Array(n_customers);

    var i,j;

    for(j=0;j<n_customers; j++){
        customers_indexes[j] = j;
    }

    var solution = new Array(n_customers);

    var cust_requests_sum = [];
    for (i = 0; i < n_stores; i++) {
        cust_requests_sum[i] = 0;
    }

    var z=0;

    for (j = 0; j < n_customers; j++) {


        var current_cust_index = customers_indexes[j];

        var requests_customer = new Array(n_stores);
        for (i = 0; i < n_stores; i++) {
            requests_customer[i] = new Array(2);
            requests_customer[i][CONSTANT_KEY] = i;
            requests_customer[i][CONSTANT_VALUE] = requests[i][current_cust_index];
        }

        // ordino in modo ascendente
        requests_customer.sort(function (a, b) {
            return (a[CONSTANT_VALUE] ==  b[CONSTANT_VALUE]) ? 0 : (a[CONSTANT_VALUE] < b[CONSTANT_VALUE]) ? -1 : 1;
        });

        var random_RCL_element = Math.floor((Math.random() * k));

        var s = 0;
        while(s < n_stores) {
            var index_store = requests_customer[(s + random_RCL_element) % n_stores][CONSTANT_KEY];
            if (cust_requests_sum[index_store] + requests[index_store][current_cust_index] <= capacities[index_store]) {
                solution[current_cust_index] = index_store;
                cust_requests_sum[index_store] += requests[index_store][current_cust_index];
                z += costs[index_store][current_cust_index];
                break;
            }
            s++;
        }

    }

    return {
        sol: solution,
        cost: z,
        stores_req_sum: cust_requests_sum
    };
}


function isFeasible(solutionArray, instance){

    var i,j;

    // controllo che tutti i clienti siano serviti e lo siano da un solo magazzino.
    for(j=0;j<instance.n_customers;j++){
        if(solutionArray[j]==undefined){
            return false;
        }
    }

    // Check stores capacities
    var stores_req_sum = new Array(instance.n_stores);

    for(i=0;i<instance.n_stores;i++){
        stores_req_sum[i] = 0;
    }

    for(j=0;j<instance.n_customers;j++){
        stores_req_sum[solutionArray[j]] += instance.requests[solutionArray[j]][j];
    }

    for(i=0;i<instance.n_stores;i++){
        if(stores_req_sum[i] > instance.capacities[i]){
            return false;
        }
    }

    return true;
}