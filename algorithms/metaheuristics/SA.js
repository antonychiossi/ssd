var etime;
var eiters;
var timer = {};

function isTimeOver() {
    timer.stop = performance.now();
    return timer.stop - timer.start < etime;
}

this.onmessage = function(params) {
    if(params.data.solution != undefined && params.data.instance != undefined){
        if(params.data.exec_params != undefined) {
            etime = params.data.exec_params.time;
            eiters = params.data.exec_params.iters;

            if(params.data.opt != undefined){
                timer.start = performance.now();
                if(params.data.opt == 10){
                    var t0 = performance.now();
                    //console.log(params.data.solution);
                    var sol = SA(params.data.solution, params.data.instance);
                    //console.log(sol);
                    var t1 = performance.now();
                    postMessage({task: "sa-10", result: sol, exec_time: (t1 - t0)});
                }
            }
        }
    }
};


function SA(solution, instance) {

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    //var costs = instance.costs;
    var requests = instance.requests;
    //var capacities = instance.capacities;


    var k = 1.0;
    var iter = 0;
    var t =  950 + Math.floor((Math.random() * 100) + 1);
    var alpha = 0.9;

    var bestSolution = solution.sol.slice();
    var bestCost = solution.cost;

    var current_sol = bestSolution.slice();
    var current_cost = bestCost;

    var stores_req_sum = solution.stores_req_sum.slice();

    var m,j,i;

    do{

        //individuo vicino data la sol
        var result = gap10optSA(current_sol, current_cost, stores_req_sum, instance);

        // se vicino è migliore
        if(result.cost < current_cost){

            j = result.move.j;
            i = result.move.i;
            stores_req_sum[current_sol[j]] -= requests[current_sol[j]][j];
            stores_req_sum[i] += requests[i][j];
            current_sol[j] = i;
            current_cost = result.cost;

            if(current_cost < bestCost){
                bestSolution = current_sol.slice();
                bestCost = current_cost;
            }

        } else if(result.cost > current_cost){

            var p = Math.exp(-(result.cost-current_cost)/(k*t));

            if(Math.random() < p){

                j = result.move.j;
                i = result.move.i;
                stores_req_sum[current_sol[j]] -= requests[current_sol[j]][j];
                stores_req_sum[i] += requests[i][j];
                current_sol[j] = i;
                current_cost = result.cost;

            }
        }

        // aggiorno la t
        if(iter % 300 == 0)
            t*=alpha;

        iter++;

    } while (iter < eiters && isTimeOver());


    var bestSolution_stores_req_sum = new Array(n_stores);
    for(j=0;j<n_customers;j++){
        bestSolution_stores_req_sum[j] = 0;
    }
    for(j=0;j<n_customers;j++){
        bestSolution_stores_req_sum[bestSolution[j]] += requests[bestSolution[j]][j];
    }

    //console.log(bestCost);
    return {
            sol: bestSolution,
            cost: bestCost,
            stores_req_sum:bestSolution_stores_req_sum
        };
}


function gap10optSA(solutionArray, cost, storeSum, instance) {

    var customerIndex = Math.floor((Math.random() * instance.n_customers));
    var storeIndex = Math.floor((Math.random() * instance.n_stores));

    var currentStore = solutionArray[customerIndex];

    var newCost = cost;
    var move;

    if (storeIndex != currentStore && storeSum[storeIndex] + instance.requests[storeIndex][customerIndex] <= instance.capacities[storeIndex]) {
        move = {j: customerIndex, i: storeIndex};

        newCost = cost - instance.costs[currentStore][customerIndex] + instance.costs[storeIndex][customerIndex];

    }

    return {
        move: move,
        cost: newCost
    };

}
