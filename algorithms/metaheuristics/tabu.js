var etime;
var eiters;
var timer = {};

function isTimeOver() {
    timer.stop = performance.now();
    return timer.stop - timer.start < etime;
}

this.onmessage = function(params) {
    if(params.data.solution != undefined && params.data.instance != undefined){

        etime = params.data.exec_params.time;
        eiters = params.data.exec_params.iters;
        timer.start = performance.now();

        var t0 = performance.now();
        var sol = tabu(params.data.solution, params.data.instance);
        var t1 = performance.now();
        postMessage({task: "tabu", result: sol, exec_time: (t1 - t0)});
    }
};

function tabu(solution, instance){

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    var costs = instance.costs;
    var requests = instance.requests;
    var capacities = instance.capacities;

    var MAX_ITER = eiters;
    var TABU_TENURE = 2*n_stores;
    var iter = 0;

    var i, j;

    var tabuList = [];
    for(i=0; i<n_stores; i++) {
        tabuList[i] = new Array(n_customers);
        for(j=0;j<n_customers; j++){
            tabuList[i][j] = 0;
        }
    }

    var bestSolution = solution.sol.slice();
    var bestCost = solution.cost;

    var currentSolution = bestSolution.slice();
    var currentCost = bestCost;


    var stores_req_sum = solution.stores_req_sum.slice();

    // tabu search core
    for(iter = 0; iter < MAX_ITER && isTimeOver(); iter++){
        var iBest = undefined;
        var jBest = undefined;
        var bestNeighbourCost = Number.MAX_VALUE;

        // find the best 1-0 opt neighbour
        for(j=0; j<n_customers; j++){
            var currentStore = currentSolution[j];
            for(i=0; i<n_stores; i++){

                if(i==currentStore){ continue; }

                var newCost = currentCost - costs[currentStore][j] + costs[i][j];

                if(stores_req_sum[i]+requests[i][j] <= capacities[i] && iter >= tabuList[i][j] && newCost < bestNeighbourCost){

                    iBest = i;
                    jBest = j;
                    bestNeighbourCost = newCost;

                }

            }
        }

        if(iBest == undefined || jBest == undefined){
            console.log("[tabu] Terminating at "+iter+"/"+MAX_ITER);
            break;
        }

        currentCost = bestNeighbourCost;

        stores_req_sum[currentSolution[jBest]] -= requests[currentSolution[jBest]][jBest];
        stores_req_sum[iBest] += requests[iBest][jBest];


        currentSolution[jBest] = iBest;

        // aggiungo mosse alla tb list
        tabuList[iBest][jBest] = iter+TABU_TENURE;


        if(currentCost < bestCost){
            bestSolution = currentSolution.slice();
            bestCost = currentCost;
        }
    }

    var bestSolution_stores_req_sum = new Array(n_stores);
    for(j=0;j<n_customers;j++){
        bestSolution_stores_req_sum[j] = 0;
    }
    for(j=0;j<n_customers;j++){
        bestSolution_stores_req_sum[bestSolution[j]] += requests[bestSolution[j]][j];
    }

    return {
        sol: bestSolution,
        cost: bestCost,
        stores_req_sum: bestSolution_stores_req_sum
    };

}
