
var etime;
var eiters;
var timer = {};

function isTimeOver() {
    timer.stop = performance.now();
    return timer.stop - timer.start < etime;
}

if( 'function' === typeof importScripts) {
    importScripts('../locals/10opt.js', '../locals/11opt.js');

    this.onmessage = function (params) {
        if (params.data.solution != undefined && params.data.instance != undefined) {
            if(params.data.exec_params != undefined) {
                etime = params.data.exec_params.time;
                eiters = params.data.exec_params.iters;

                //console.log("@@@@@@@ " + params.data.exec_params.toSource());

                if (params.data.opt != undefined) {
                    console.log("######## " + etime);
                    timer.start = performance.now();
                    if (params.data.opt == 10) {
                        var t0_10 = performance.now();
                        var sol_10 = ILS(params.data.solution, params.data.instance, 10);
                        var t1_10 = performance.now();
                        postMessage({task: "ils-10", result: sol_10, exec_time: (t1_10 - t0_10)});
                    } else {
                        var t0_11 = performance.now();
                        var sol_11 = ILS(params.data.solution, params.data.instance, 11);
                        var t1_11 = performance.now();
                        postMessage({task: "ils-11", result: sol_11, exec_time: (t1_11 - t0_11)});
                    }
                }
            }
        }
    };
}


function ILS(solution, instance, opt_type){

    var n_stores = instance.n_stores;
    var n_customers = instance.n_customers;
    var disturbation_percentage = 0.5 + Math.floor((Math.random() * 10) + 1) / 100;

    // perturbazione matrice dei costi
    var perturbed_costs = new Array(n_stores);
    for(var s=0; s < n_stores; s++){
        perturbed_costs[s] = new Array(n_customers);
    }

    var opt;
    var iterations = 0;
    if(opt_type == 10){
        opt = gap10opt;
    }else{
        opt = executeOpts;
    }

    do {

        var initial_costs = instance.costs;
        solution = opt(solution, instance);

        perturbed_solution = {
            sol: solution.sol.slice(),
            cost: solution.cost,
            stores_req_sum: solution.stores_req_sum.slice()
        };

        instance.costs = perturbMatrix(initial_costs, perturbed_costs, n_stores, n_customers, disturbation_percentage);
        var perturbed_solution = opt(perturbed_solution, instance);

        instance.costs = initial_costs;

        perturbed_solution.cost = z(perturbed_solution.sol, instance);

        if(perturbed_solution.cost < solution.cost){
            solution = perturbed_solution;
        }

        iterations++;

    } while(iterations < eiters && isTimeOver());

    //console.log(solution);
    return solution;
}

function z(solutionArray, instance){
    var cost = 0;

    for(var j=0;j<instance.n_customers;j++){
        cost += instance.costs[solutionArray[j]][j];
    }

    return cost;
}

function perturbMatrix(initial_matrix, new_matrix, rows, columns, factor) {

    for (var i = 0; i < rows; i++) {
        for (var j = 0; j < columns; j++) {
            var percentage = (Math.random() * (factor/2));
            if (Math.random() <= 0.5) {
                percentage *= -1;
            }
            new_matrix[i][j] = initial_matrix[i][j] + (initial_matrix[i][j] * percentage);
        }
    }

    return new_matrix;
}