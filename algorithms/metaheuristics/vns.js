/**
 * Created by heis on 12/6/16.
 */

var etime;
var eiters;
var timer = {};

function isTimeOver() {
    timer.stop = performance.now();
    return timer.stop - timer.start < etime;
}

if( 'function' === typeof importScripts) {
    importScripts('../locals/10opt.js', '../locals/11opt.js');
    importScripts('https://cloud.github.com/downloads/kpozin/jquery-nodom/jquery.nodom.js');

    this.onmessage = function (params) {
        if (params.data.solution != undefined && params.data.instance != undefined) {

            etime = params.data.exec_params.time;
            eiters = params.data.exec_params.iters;
            timer.start = performance.now();

            var t0 = performance.now();
            var sol = vns(params.data.solution, params.data.instance);
            var t1 = performance.now();
            postMessage({task: "vns-11", result: sol, exec_time: (t1 - t0)});
        }
    };
}

function vns(solution, instance){

    var iter = 0;

    solution = vnd(solution, instance);

    do {

        var new_sol = gap21move(copy(solution),instance);

        new_sol = vnd(new_sol, instance);

        if(new_sol.cost < solution.cost) {
            solution = new_sol;
        }

        iter++;
    } while(iter < eiters && isTimeOver());

    return solution;

}

function vnd(solution, instance){

    var k = 0;
    var local_searches = [gap10opt, intersectMoves];

    do {

        var current_cost = solution.cost;

        solution = local_searches[k](solution, instance);

        if(solution.cost < current_cost){
            k=0;
        } else {
            k++;
        }

    } while(local_searches.length > k);

    return solution;

}


function gap21move(solution, instance){

    var n_customers = new Array(instance.n_customers);
    var costs = instance.costs;

    var improved = false;

    $.each(n_customers, function(index, value) {
        var couple_index1 = index;
        var current_store_i = solution.sol[couple_index1];

        if(couple_index1 >= instance.n_customers-2 && improved){
            return false;
        }

        $.each(n_customers, function(index, value) {
            var couple_index2 = index;
            if(couple_index2 < couple_index1+1)
                return true;

            if(couple_index2 >= instance.n_customers-1 && improved){
                return false;
            }

            var current_store_j = solution.sol[couple_index2];

            // se sono nello stesso store: continue;
            if(current_store_i != current_store_j) {
                return true;
            }

            $.each(n_customers, function(index, value) {

                var only_customer = index;
                var current_store_k = solution.sol[only_customer];

                if(only_customer >= instance.n_customers && improved){
                    return false;
                }

                // se il solitario è dello stesso store degli altri: continue;
                if(only_customer == couple_index1 || only_customer == couple_index2 || current_store_k == current_store_i){
                    return true;
                }

                var new_cost = solution.cost
                    - costs[current_store_i][couple_index1] - costs[current_store_j][couple_index2] - costs[current_store_k][only_customer]
                    + costs[current_store_k][couple_index1] + costs[current_store_k][couple_index2] + costs[current_store_i][only_customer];


                //test SWAP
                if( new_cost < solution.cost &&
                    // rimuovo i 2 e aggiungo il solitario
                    solution.stores_req_sum[current_store_i] - instance.requests[current_store_i][couple_index1] - instance.requests[current_store_i][couple_index2]
                    + instance.requests[current_store_i][only_customer] <= instance.capacities[current_store_i] &&
                    // rimuovo il solitario e aggiungo gli altri
                    solution.stores_req_sum[current_store_k] - instance.requests[current_store_k][only_customer] + instance.requests[current_store_k][couple_index1]
                    + instance.requests[current_store_k][couple_index2] <= instance.capacities[current_store_k]
                ) {


                    solution.cost = new_cost;

                    solution.stores_req_sum[current_store_i] += - instance.requests[current_store_i][couple_index1] - instance.requests[current_store_i][couple_index2]
                        + instance.requests[current_store_i][only_customer];

                    solution.stores_req_sum[current_store_k] += - instance.requests[current_store_k][only_customer] + instance.requests[current_store_k][couple_index1]
                        + instance.requests[current_store_k][couple_index2];

                    solution.sol[couple_index1] = current_store_k;
                    solution.sol[couple_index2] = current_store_k;

                    solution.sol[only_customer] = current_store_i;

                    improved = true;

                    return false;
                }

                if(improved) { return false; }

            }); // for only_customer

            if(improved) { return false; }
        }); // for couple_index2

        if(improved) { return false; }
    }); // for couple_index1

    return solution;

}

function copy(solution){
    return {
        sol: solution.sol.slice(),
        cost: solution.cost,
        stores_req_sum: solution.stores_req_sum.slice()
    };
}
